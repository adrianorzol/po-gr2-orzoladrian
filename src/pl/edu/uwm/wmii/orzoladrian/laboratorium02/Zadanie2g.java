package pl.edu.uwm.wmii.orzoladrian.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2g {
    public static void main(String[] args) {
        int n, prawy, lewy;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        if(n>= 1 && n <= 100)
        {
            int[] a = new int[n];
            generacja(a);
            lewy = odczyt.nextInt();
            prawy = odczyt.nextInt();
            System.out.println("Tablica przed zmiana:");
            for(int h=0; h<a.length; h++)
            {
                System.out.println(h+1 + " " + a[h]);
            }
            odwrocFragment(a, prawy, lewy);
        }
        else System.out.println("n się nie zgadza");
    }
    public static int[] generacja(int tab[])
    {
        Random generuj = new Random();
        for(int i=0; i<tab.length; i++)
        {
            tab[i] = generuj.nextInt(1999)-999;
        }
        return tab;
    }
    public static void odwrocFragment(int tab[], int prawy, int lewy)
    {
        int pom;
        for(int i=0; i<(prawy-lewy)/2+1; i++)
        {
            pom = tab[i+lewy-1];
            tab[i+lewy-1] = tab[prawy-i-1];
            tab[prawy-i-1] = pom;
        }
            System.out.println("Po zamianie:");
            for(int h=0; h<tab.length; h++)
            {
                System.out.println(h+1 + " " + tab[h]);
            }
    }
}
