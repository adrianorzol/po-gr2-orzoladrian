package pl.edu.uwm.wmii.orzoladrian.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2f {
    public static void main(String[] args) {
        int n;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        if(n>= 1 && n <= 100)
        {
            int[] a = new int[n];
            generacja(a);
            System.out.println("Zmodyfikowana tablica:");
            signum(a);
        }
        else System.out.println("n się nie zgadza");
    }
    public static int[] generacja(int tab[])
    {
        Random generuj = new Random();
        for(int i=0; i<tab.length; i++)
        {
            tab[i] = generuj.nextInt(1999)-999;
        }
        return tab;
    }
    public static void signum(int tab[])
    {
        for(int i=0; i<tab.length; i++)
        {
            if(tab[i] > 0) tab[i]=1;
            if(tab[i] < 0) tab[i]=-1;
            System.out.println(tab[i]);
        }
    }
}
