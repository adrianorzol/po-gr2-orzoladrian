package pl.edu.uwm.wmii.orzoladrian.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1d {
    public static void main(String[] args) {
        int n, tab;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        if(n>= 1 && n <= 100)
        {
            tab = n;
            int[] a = new int[tab];
            Random generuj = new Random();
            for(int i=0; i<tab; i++)
            {
                a[i] = generuj.nextInt(1999)-999;
            }
            int wynik1 = 0, wynik2 = 0;
            for(int i=0; i<tab; i++)
            {
                if(a[i] > 0) wynik1+=a[i];
                if(a[i] < 0) wynik2+=a[i];
            }
            System.out.println("Suma dodatnich wynosi " + wynik1 + ", a ujemnych " + wynik2);
        }
        else System.out.println("n się nie zgadza");
    }
}
