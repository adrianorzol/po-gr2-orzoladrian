package pl.edu.uwm.wmii.orzoladrian.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1c {
    public static void main(String[] args) {
        int n, tab;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        if(n>= 1 && n <= 100)
        {
            tab = n;
            int[] a = new int[tab];
            Random generuj = new Random();
            for(int i=0; i<tab; i++)
            {
                a[i] = generuj.nextInt(1999)-999;
            }
            int wynik1 = a[0], wynik2 = 0;
            for(int i=1; i<tab; i++)
            {
                if (a[i] > wynik1)
                {
                    wynik1 = a[i];
                }
            }
            for(int i=0; i<tab; i++)
            {
                if (a[i] == wynik1)
                {
                    wynik2++;
                }
            }
            System.out.println("Najwiekszym elementem jest " + wynik1 + " i wystepuje " + wynik2 + " raz(y)");
        }
        else System.out.println("n się nie zgadza");
    }
}
