package pl.edu.uwm.wmii.orzoladrian.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1f {
    public static void main(String[] args) {
        int n, tab;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        if(n>= 1 && n <= 100)
        {
            tab = n;
            int[] a = new int[tab];
            Random generuj = new Random();
            for(int i=0; i<tab; i++)
            {
                a[i] = generuj.nextInt(1999)-999;
            }
            for(int i=0; i<tab; i++)
            {
                if(a[i] > 0) a[i]=1;
                if(a[i] < 0) a[i]=-1;
                System.out.println(a[i]);
            }

        }
        else System.out.println("n się nie zgadza");
    }

}
