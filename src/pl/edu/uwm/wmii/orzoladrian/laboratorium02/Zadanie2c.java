package pl.edu.uwm.wmii.orzoladrian.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2c {
    public static void main(String[] args) {
        int n;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        if(n>= 1 && n <= 100)
        {
            int[] a = new int[n];
            generacja(a);
            System.out.println("Najwieksze jest " + Maksymalny(a) + " i znajduje sie w tablicy " + ileMaksymalnych(a) + " raz(y)");
        }
        else System.out.println("n się nie zgadza");
    }
    public static int[] generacja(int tab[])
    {
        Random generuj = new Random();
        for(int i=0; i<tab.length; i++)
        {
            tab[i] = generuj.nextInt(1999)-999;
        }
        return tab;
    }
    public static int Maksymalny(int tab[])
    {
        int wynik=tab[0];
        for(int i=1; i<tab.length; i++)
        {
            if (tab[i] > wynik)
            {
                wynik = tab[i];
            }
        }
        return wynik;
    }
    public static int ileMaksymalnych(int tab[])
    {
        int wynik = 0;
        for(int i=0; i<tab.length; i++)
        {
            if (tab[i] == Maksymalny(tab))
            {
                wynik++;
            }
        }
        return wynik;
    }
}
