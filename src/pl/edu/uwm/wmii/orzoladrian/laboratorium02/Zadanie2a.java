package pl.edu.uwm.wmii.orzoladrian.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2a {
    public static void main(String[] args) {
        int n;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        if(n>= 1 && n <= 100)
        {
            int[] a = new int[n];
            generacja(a);
            System.out.println("Parzystych jest " + ileParzystych(a) + " a nieparzystych " + ileNieparzystych(a));
        }
        else System.out.println("n się nie zgadza");
    }
    public static int[] generacja(int tab[])
    {
        Random generuj = new Random();
        for(int i=0; i<tab.length; i++)
        {
            tab[i] = generuj.nextInt(1999)-999;
        }
        return tab;
    }
    public static int ileNieparzystych(int tab[])
    {
        int wynik=0;
        for(int i=0; i<tab.length; i++)
        {
                if (tab[i] % 2 != 0) wynik += 1;
        }
        return wynik;
    }
    public static int ileParzystych(int tab[])
    {
        int wynik=0;
        for(int i=0; i<tab.length; i++)
        {
                if (tab[i] % 2 == 0) wynik += 1;
        }
        return wynik;
    }
}
