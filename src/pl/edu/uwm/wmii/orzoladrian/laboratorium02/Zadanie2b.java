package pl.edu.uwm.wmii.orzoladrian.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2b {
    public static void main(String[] args) {
        int n;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        if(n>= 1 && n <= 100)
        {
            int[] a = new int[n];
            generacja(a);
            System.out.println("Dodatnich jest " + ileDodatnich(a) + ", ujemnych " + ileUjemnych(a) + ", a zerowych " + ileZerowych(a));
        }
        else System.out.println("n się nie zgadza");
    }
    public static int[] generacja(int tab[])
    {
        Random generuj = new Random();
        for(int i=0; i<tab.length; i++)
        {
            tab[i] = generuj.nextInt(1999)-999;
        }
        return tab;
    }
    public static int ileDodatnich(int tab[])
    {
        int wynik=0;
        for(int i=0; i<tab.length; i++)
        {
            if (tab[i] > 0) wynik ++;
        }
        return wynik;
    }
    public static int ileUjemnych(int tab[])
    {
        int wynik=0;
        for(int i=0; i<tab.length; i++)
        {
            if (tab[i] < 0) wynik ++;
        }
        return wynik;
    }
    public static int ileZerowych(int tab[])
    {
        int wynik=0;
        for(int i=0; i<tab.length; i++)
        {
            if (tab[i] == 0) wynik ++;
        }
        return wynik;
    }
}
