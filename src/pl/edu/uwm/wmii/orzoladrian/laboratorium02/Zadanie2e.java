package pl.edu.uwm.wmii.orzoladrian.laboratorium02;

import java.util.Scanner;
import java.util.List;
import java.util.Random;
import java.util.ArrayList;
import java.util.Arrays;
public class Zadanie2e {
    public static void main(String[] args) {
        int n;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        if(n>= 1 && n <= 100)
        {
            int[] a = new int[n];
            generacja(a);
            System.out.println("Długośc fragmentu dodatniego to: " + dlugoscMaksymalnegoCiaguDodatnich(a));
        }
        else System.out.println("n się nie zgadza");
    }
    public static int[] generacja(int tab[])
    {
        Random generuj = new Random();
        for(int i=0; i<tab.length; i++)
        {
            tab[i] = generuj.nextInt(1999)-999;
        }
        return tab;
    }
    public static int dlugoscMaksymalnegoCiaguDodatnich(int[] tab)
    {
        int wynik = 0;
        List<Integer> pomoc = new ArrayList<Integer>();
        pomoc.add(0);
        int maks = pomoc.get(0);
        for (int i = 0; i < tab.length; i++)
        {
            if (tab[i] > 0)
            {
                wynik++;
                if(i==tab.length-1)
                {
                    pomoc.add(wynik);
                }
            }
            else if(tab[i]<=0)
            {
                pomoc.add(wynik);
                wynik=0;
            }
        }
        for (int i = 0; i < pomoc.size(); i++) {
            if (maks < pomoc.get(i)) {
                maks = pomoc.get(i);
            }
        }
        return maks;
    }
}
