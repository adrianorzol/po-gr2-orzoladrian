package pl.edu.uwm.wmii.orzoladrian.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1g {
    public static void main(String[] args) {
        int n, tab, lewy, prawy, pom;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        if(n>= 1 && n <= 100)
        {
            tab = n;
            int[] a = new int[tab];
            Random generuj = new Random();
            for(int i=0; i<tab; i++)
            {
                a[i] = generuj.nextInt(1999)-999;
                System.out.println(i+1 + " " + a[i]);
            }
            lewy = odczyt.nextInt();
            prawy = odczyt.nextInt();
            for(int i=0; i<(prawy-lewy)/2+1; i++)
            {
                pom = a[i+lewy-1];
                a[i+lewy-1] = a[prawy-i-1];
                a[prawy-i-1] = pom;
            }
            System.out.println("Po zamianie:");
            for(int i=0; i<tab; i++)
            {
                System.out.println(i+1 + " " + a[i]);
            }
        }
        else System.out.println("n się nie zgadza");
    }
}
