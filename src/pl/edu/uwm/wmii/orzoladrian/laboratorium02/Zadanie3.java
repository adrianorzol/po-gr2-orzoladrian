package pl.edu.uwm.wmii.orzoladrian.laboratorium02;
import java.util.*;
public class Zadanie3 {
    public static void main(String[] args)
    {
        Scanner odczyt=new Scanner(System.in);
        int m=odczyt.nextInt();
        int n=odczyt.nextInt();
        int k=odczyt.nextInt();
        if((n>= 1 && n <= 10) && (m>= 1 && m <= 10) && (k>= 1 && k <= 10))
        {
            Random rand = new Random();
            int[][] macierzA = new int[m][n];
            int[][] macierzB = new int[n][k];
            int[][] macierzC = new int[m][k];
            for (int i = 0; i < macierzA.length; i++)
            {
                for (int j = 0; j < macierzA[0].length; j++)
                {
                    macierzA[i][j] = rand.nextInt(20);
                }
            }
            for (int i = 0; i < macierzB.length; i++)
            {
                for (int j = 0; j < macierzB[0].length; j++)
                {
                    macierzB[i][j] = rand.nextInt(20);
                }
            }

            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < k; j++)
                {
                    for (int a = 0; a < n; a++)
                    {
                        macierzC[i][j] += (macierzA[i][a] * macierzB[a][j]);
                    }
                }
            }
            System.out.println("Macierz A");
            wyswietlanie(macierzA);
            System.out.println("Macierz B");
            wyswietlanie(macierzB);
            System.out.println("Macierz C");
            wyswietlanie(macierzC);
        }
        else System.out.println("Niewlasciwe zmienne");
    }
    public static void wyswietlanie(int macierz[][])
    {
        for(int i=0;i<macierz.length;i++)
        {
            for (int j = 0; j < macierz[0].length; j++)
                System.out.print(macierz[i][j] + " ");
            System.out.println();
        }

    }
}
