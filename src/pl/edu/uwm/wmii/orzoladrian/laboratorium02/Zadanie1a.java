package pl.edu.uwm.wmii.orzoladrian.laboratorium02;

import java.util.Scanner;
import java.util.Random;

public class Zadanie1a
    {
    public static void main(String[] args) {
    int n, tab;
    Scanner odczyt = new Scanner(System.in);
    n = odczyt.nextInt();
    if(n>= 1 && n <= 100)
    {
        tab = n;
        int[] a = new int[tab];
        Random generuj = new Random();
        for(int i=0; i<tab; i++)
        {
            a[i] = generuj.nextInt(1999)-999;
        }
        int wynik1 = 0, wynik2 = 0;
        for(int i=0; i<tab; i++)
        {
            if(a[i]%2 == 0) wynik1+=1;
            else wynik2+=1;
        }
        System.out.println("Parzystych jest " + wynik1 + " a nieparzystych " + wynik2);
    }
    else System.out.println("n się nie zgadza");
    }

}
