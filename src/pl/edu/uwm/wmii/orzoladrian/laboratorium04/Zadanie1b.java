package pl.edu.uwm.wmii.orzoladrian.laboratorium04;

public class Zadanie1b {
    public static String napis = "kormoran";
    public static String znak = "or";
    public static void main(String[] args) {
        System.out.println("ciag znakow '" + znak + "' wystepuje w wyrazie '" + napis + "' " + countSubChar(napis, znak) + " raz(y)");
    }
    public static int countSubChar(String str, String subStr)
    {
        int wynik = 0;
        for(int i=0, j = subStr.length(); i<str.length() && j<str.length()+1; i++, j++)
        {
            if(str.substring(i, j).equals(subStr))
            wynik++;
        }
        return wynik;
    }
}
