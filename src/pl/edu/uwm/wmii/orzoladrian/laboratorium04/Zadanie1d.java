package pl.edu.uwm.wmii.orzoladrian.laboratorium04;

import java.util.Scanner;

public class Zadanie1d {
    public static void main(String[] args) {
        String napis;
        int numer;
        Scanner odczyt = new Scanner(System.in);
        napis = odczyt.next();
        numer = odczyt.nextInt();
        System.out.println(napis);
        System.out.println("Powtorzony " + numer + " razy napis '" + napis + "' to '" + repeat(napis, numer) + "'");
    }
    public static String repeat(String str, int liczba)
    {
        StringBuilder wynik= new StringBuilder();
        for(int i=0; i<liczba; i++)
            wynik.append(str);
        return wynik.toString();
    }
}
