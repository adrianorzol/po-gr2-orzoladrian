package pl.edu.uwm.wmii.orzoladrian.laboratorium04;
public class Zadanie1a {
    public static String napis = "kajak";
    public static char znak = 'j';
    public static void main(String[] args) {
    System.out.println("znak '" + znak + "' wystepuje w wyrazie " + napis + " " + countChar(napis, znak) + " raz(y)");
    }
    public static int countChar(String str, char c)
    {
        int wynik = 0;
        for(int i=0; i<str.length(); i++)
        {
            if(str.charAt(i) == c)
                wynik++;
        }
        return wynik;
    }
}
