package pl.edu.uwm.wmii.orzoladrian.laboratorium04;
import java.util.Scanner;
public class Zadanie1c {
    public static void main(String[] args) {
        String napis;
        Scanner odczyt = new Scanner(System.in);
        napis = odczyt.next();
        System.out.println(napis);
        System.out.println("Srodek wyrazu '" + napis + "' to '" + middle(napis) + "'");
    }
    public static String middle(String str)
    {
        String wynik="";
        if(str.length()%2 != 0)
        {
            for (int i = 0; i < str.length(); i++)
            {
                if (i + 1 == str.length() / 2)
                {
                    wynik += str.charAt(i+1);
                }
            }
        }
        else
        {
            for (int i = 0; i < str.length(); i++)
            {
                if (i + 1 == str.length() / 2)
                {
                    wynik += str.charAt(i);
                    wynik+= str.charAt(i+1);
                }
            }
        }
        return wynik;
    }
}

