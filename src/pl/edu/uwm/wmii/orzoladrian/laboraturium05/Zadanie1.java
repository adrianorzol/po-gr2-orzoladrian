package pl.edu.uwm.wmii.orzoladrian.laboraturium05;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Zadanie1 {
    public static void main(String[] args) {
        ArrayList<Integer> lista = new ArrayList<>();
        lista.add(1);
        lista.add(2);
        lista.add(3);
        lista.add(7);
        ArrayList<Integer> lista2 = new ArrayList<>();
        lista2.add(4);
        lista2.add(5);
        lista2.add(6);

        System.out.println(append(lista, lista2));
        System.out.println(merge(lista, lista2));
        System.out.println(reversed(lista));
        reverse(lista2);
        System.out.println(lista2);
    }
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> c = new ArrayList<>();
            c.addAll(a);
            c.addAll(b);
        return c;
    }
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> c = new ArrayList<>();
        if(a.size()>b.size())
        {
            for(int i=0; i<a.size(); i++)
            {
                c.add(a.get(i));
                if(i != b.size())
                    c.add(b.get(i));
            }
        }
        else
        {
            for(int i=0; i<b.size(); i++)
            {
                if(i != a.size())
                    c.add(a.get(i));
                c.add(b.get(i));
            }
        }
        return c;
    }
    public static ArrayList<Integer> reversed(ArrayList<Integer> a)
    {
        ArrayList<Integer> c = new ArrayList<>();
        Collections.reverse(a);
        c.addAll(a);
        return c;
    }
    public static void reverse(ArrayList<Integer> a)
    {
        Collections.reverse(a);
    }
}
