package pl.edu.uwm.wmii.orzoladrian.laboratorium01;

import java.util.Scanner;

public class Zadanie7 {
    public static void main(String[] args) {
        int n;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        double c, b;
        int wynik = 0;
        double [] a = new double[n];
        for (int i=0; i<n; i++)
        {
            a[i] = odczyt.nextDouble();
        }
        for (int i=1; i<n; i++)
        {
            c = a[i-1];
            b = a[i];
            if(c>0 && b>0)
                wynik+=1;
        }
        System.out.println("Liczba par to " + wynik);
    }
}
