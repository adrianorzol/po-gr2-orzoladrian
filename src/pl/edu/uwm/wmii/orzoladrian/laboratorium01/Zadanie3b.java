package pl.edu.uwm.wmii.orzoladrian.laboratorium01;

import java.util.Scanner;

public class Zadanie3b {
    public static void main(String[] args) {
        int n;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        int wynik1 = 0;
        int [] a = new int[n];
        for (int i=0; i<n; i++)
        {
            a[i] = odczyt.nextInt();
        }
        for (int i=0; i<n; i++)
        {
            if((a[i])%3 == 0 && (a[i])%5 != 0)
                wynik1+=1;
        }
        System.out.println("Liczb podzielnych przez 3 i niepodzielnych przez 5 jest " + wynik1);
    }
}
