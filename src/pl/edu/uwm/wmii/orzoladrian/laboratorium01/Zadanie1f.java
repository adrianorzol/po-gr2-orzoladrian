package pl.edu.uwm.wmii.orzoladrian.laboratorium01;

import java.util.Scanner;

public class Zadanie1f {
    public static void main(String[] args) {
        int n;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        double wynik = 0;
        double [] a = new double[n];
        for (int i=0; i<n; i++)
        {
            a[i] = odczyt.nextDouble();
        }
        for (int i=0; i<n; i++)
        {
            wynik+=Math.pow(a[i], 2);
        }
        System.out.println(wynik);
    }
}
