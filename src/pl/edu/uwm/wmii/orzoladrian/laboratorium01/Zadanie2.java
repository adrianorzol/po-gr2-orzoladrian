package pl.edu.uwm.wmii.orzoladrian.laboratorium01;

import java.util.Scanner;
public class Zadanie2 {
    public static void main(String[] args) {
        int n;
        double pom;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        double [] a = new double[n];
        for (int i=0; i<n; i++)
        {
            a[i] = odczyt.nextDouble();
        }
        for(int i=0; i<n/2; i++)
        {
            pom = a[i];
            a[i] = a[n-1-i];
            a[n-1-i] = pom;
        }
        int m = n - 1;
        for(int i=0; i<m/2; i++)
        {
            pom = a[i];
            a[i] = a[m-1-i];
            a[m-1-i] = pom;
        }
        for (int i=0; i<n; i++)
        {
            System.out.println(a[i]);
        }
    }
}
