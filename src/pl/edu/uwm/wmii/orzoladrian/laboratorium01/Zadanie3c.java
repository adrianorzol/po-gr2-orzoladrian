package pl.edu.uwm.wmii.orzoladrian.laboratorium01;

import java.util.Scanner;

public class Zadanie3c {
    public static void main(String[] args) {
        int n;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        int wynik1 = 0;
        int [] a = new int[n];
        for (int i=0; i<n; i++)
        {
            a[i] = odczyt.nextInt();
        }
        for (int i=0; i<n; i++)
        {
            if(Math.sqrt(a[i])%2 == 0)
                wynik1+=1;
        }
        System.out.println("Liczb, które są kwadratami liczby parzystej jest " + wynik1);
    }
}
