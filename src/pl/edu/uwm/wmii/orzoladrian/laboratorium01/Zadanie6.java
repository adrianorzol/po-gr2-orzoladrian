package pl.edu.uwm.wmii.orzoladrian.laboratorium01;

import java.util.Scanner;

public class Zadanie6 {
    public static void main(String[] args) {
        int n;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        double [] a = new double[n];
        for (int i=0; i<n; i++)
        {
            a[i] = odczyt.nextDouble();
        }
        double wynik1 = a[0];
        for (int i = 1; i < n; i++)
        {
            if (a[i] > wynik1)
            {
                wynik1 = a[i];
            }
        }
        double wynik2 = a[0];
        for (int i = 1; i < n; i++)
        {
            if (a[i] < wynik2)
            {
                wynik2 = a[i];
            }
        }
        System.out.println("Największe jest " + wynik1);
        System.out.println("Najmniejsze jest " + wynik2);
    }
}
