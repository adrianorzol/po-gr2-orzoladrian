package pl.edu.uwm.wmii.orzoladrian.laboratorium01;
import java.lang.Math;
import java.util.Scanner;

public class Zadanie1d {
    public static void main(String[] args) {
        int n;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        double wynik = 0;
        double [] a = new double[n];
        for (int i=0; i<n; i++)
        {
            a[i] = odczyt.nextDouble();
        }
        for (int i=0; i<n; i++)
        {
            wynik+=Math.sqrt(Math.abs(a[i]));
        }
        System.out.println(wynik);
    }
}
