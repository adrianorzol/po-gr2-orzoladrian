package pl.edu.uwm.wmii.orzoladrian.laboratorium01;

import java.util.Scanner;

public class Zadanie4 {
    public static void main(String[] args) {
        int n;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        double wynik1 = 0;
        double [] a = new double[n];
        for (int i=0; i<n; i++)
        {
            a[i] = odczyt.nextDouble();
        }
        for (int i=0; i<n; i++)
        {
            if(a[i]>0)
                wynik1+=a[i];
        }
        wynik1*=2;
        System.out.println("Wynik to " + wynik1);
    }
}
