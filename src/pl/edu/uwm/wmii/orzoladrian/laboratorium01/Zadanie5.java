package pl.edu.uwm.wmii.orzoladrian.laboratorium01;

import java.util.Scanner;

public class Zadanie5 {
    public static void main(String[] args) {
        int n;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        double [] a = new double[n];
        for (int i=0; i<n; i++)
        {
            a[i] = odczyt.nextDouble();
        }
        int wynik1 = 0;
        int wynik2 = 0;
        int wynik3 = 0;
        for (int i = 0; i < n; i++)
        {
            if (a[i] > 0)
            {
                wynik1 += 1;
            }
            if (a[i] < 0)
            {
                wynik2 += 1;
            }
            if (a[i] == 0)
            {
                wynik3 += 1;
            }
        }
        System.out.println("Liczb dodatnich jest " + wynik1);
        System.out.println("Liczb ujemnych jest " + wynik2);
        System.out.println("Zer jest " + wynik3);
    }
}
