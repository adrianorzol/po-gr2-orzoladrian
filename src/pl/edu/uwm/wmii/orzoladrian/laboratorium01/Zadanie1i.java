package pl.edu.uwm.wmii.orzoladrian.laboratorium01;

import java.util.Scanner;

public class Zadanie1i {
    public static void main(String[] args) {
        int n;
        Scanner odczyt = new Scanner(System.in);
        n = odczyt.nextInt();
        double wynik = 0;
        int silnia = 1;
        double [] a = new double[n];
        for (int i=0; i<n; i++)
        {
            a[i] = odczyt.nextDouble();
        }
        for (int i=1; i<=n; i++)
        {
                silnia *= i;
            if((i)%2 == 1)
                wynik-=a[i-1]/silnia;
            else wynik+=a[i-1]/silnia;
        }
        System.out.println(wynik);
    }
}
